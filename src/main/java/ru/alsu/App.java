package ru.alsu;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;

import java.io.IOException;

public class App 
{
    public static void main( String[] args )
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            String[] urls = {"https://diary.mbc-studio.ru/", "https://dev.diary.mbc-studio.ru/"};

            for (String url : urls) {
                HttpGet httpGet = new HttpGet(url);

                long startTime = System.currentTimeMillis();
                try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                    long executionTime = System.currentTimeMillis() - startTime;

                    System.out.println("URL: " + url);
                    System.out.println("Status code: " + response.getCode());
                    System.out.println("Execution time: " + executionTime + " ms");
                    System.out.println("==============");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
